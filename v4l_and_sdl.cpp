#include "v4l_and_sdl.h"
#include <iostream>

VideoInputDevice::VideoInputDevice()
{
	try
	{
		const webcam::device_info_enumeration & enumeration = webcam::enumerator::enumerate();
		if(enumeration.count() == 0)
		{
			throw std::runtime_error("No video-devices found!");
		}
		const webcam::device_info & device_info = enumeration.get(0);
		const webcam::video_info_enumeration & video_info_enumeration = device_info.get_video_info_enumeration();
		const webcam::video_info & video_info = video_info_enumeration.get(0);
		
		
		video_settings.set_format(video_info.get_format());
		video_settings.set_resolution(video_info.get_resolution());
		video_settings.set_fps(15);
		
		device = std::make_shared<webcam::device>(1, video_settings);
		device->open();
	}
	catch(const webcam::webcam_exception & e)
	{
		std::cout << "error while initializing or opening webcam: " << e.what() << std::endl;
	}

}

VideoInputDevice::~VideoInputDevice()
{
	device->close();
}

webcam::image * VideoInputDevice::getImage()
{
	return device->read();
}

webcam::video_settings * VideoInputDevice::getSettings()
{
	return &video_settings;
}
