#ifndef _picture_dumper_h
#define _picture_dumper_h

#include <filesystem>
#include <libwebcam/webcam.h>

extern "C"
{
#include <libavcodec/avcodec.h>
}


class PictureDumper
{
	public:
	PictureDumper(std::filesystem::path filename, char * codecname, webcam::video_settings * settings);
	~PictureDumper();
	void addFrame(webcam::image * ptr);
	
	private:
	AVCodecContext * ctx;
	AVFrame * frame;
	AVPacket * packet;
	FILE * f;
};


#endif
