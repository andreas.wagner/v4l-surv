#include "image_filter.h"

#include <png.h>
#include <iostream>


ImageFilter::ImageFilter(std::filesystem::path blind_map, webcam::video_settings * settings):
	treshold(40)
{
	webcam::resolution res = settings->get_resolution();

	FILE * f = fopen(blind_map.c_str(), "rb");
	if(!f)
		throw std::runtime_error("Could not open blind_mask for ImageFilter.");
	//png_const_bytep header = (png_const_bytep) malloc(8);
	//fread((void*) header, 1, 8, f);
	
	//if(png_sig_cmp(header, 0, 8))
	//	throw std::runtime_error("File blind_map is not a .PNG.");
	
	//free((void*)header);
	
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png_ptr)
		throw std::runtime_error("Error alleocating png read-struct.");
		
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if(!info_ptr)
	{
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		throw std::runtime_error("Could not create png info-struct.");
	}
	
/*	png_infop end_ptr = png_create_info_struct(png_ptr);
	if(!end_ptr)
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		throw std::runtime_error("Could not create end png info-struct.");
	}*/
	
	png_init_io(png_ptr, f);
	//png_set_sig_bytes(png_ptr, 8);
	//png_read_info(png_ptr, info_ptr);
	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
	int bit_depth, color_type,
		filter_method, compression_type, interlace_type;
	png_get_IHDR(png_ptr, info_ptr, &width, &height,
		&bit_depth, &color_type, &interlace_type,
		&compression_type, &filter_method);
	png_bytepp row_pointers;
	row_pointers = png_get_rows(png_ptr, info_ptr);
	//png_read_end(png_ptr, info_ptr);
	
	mask = (unsigned char*) malloc(width*height);
	for(int y = 0; y < height; y++)
	{
		png_bytep row = row_pointers[y];
		unsigned char * mask_ptr = mask + width * y;
		for(int x = 0; x < width; x++)
		{
			*(mask_ptr + x) = row[x*4]; // get alpha
		}
	}
	
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	old_img = (unsigned char *) malloc(width*height);
	
	if(res.get_width() != width || res.get_height() != height)
		throw std::runtime_error("wrong resolution in mask");
}

ImageFilter::~ImageFilter()
{
	free(mask);
	free(old_img);
}

bool ImageFilter::process(webcam::image * ptr)
{
	long count = 10;
	for(int y = 0; y < height; y++)
	{
		const unsigned char * data = ptr->get_data() + y*width * 2;
		unsigned char * img_ptr = old_img + width * y;
		unsigned char * mask_ptr = mask + width * y;
		for(int x = 0; x < width; x++)
		{
			if(
				((*(data + x*2) < (*(img_ptr+x)-treshold)) ||  (*(data + x*2) > (*(img_ptr+x)+treshold)))
				&&
				(*(mask_ptr + x) > 127)
				)
			{
				count--;
			}
			*(img_ptr+x) = *(data + x*2);
		}
	}
	return count < 1;
}
