#include "picture-dumper.h"

#include <stdio.h>
#include <fcntl.h>

PictureDumper::PictureDumper(
	std::filesystem::path filename,
	char * encodername,
	webcam::video_settings * settings):
	ctx(0)
{
	webcam::resolution res = settings->get_resolution();
	
	const AVCodec *codec = avcodec_find_encoder_by_name(encodername);
	if (!codec)
	{
		throw std::runtime_error("Codec?");
	}
	ctx = avcodec_alloc_context3(codec);
	if(!ctx)
		throw std::runtime_error("Could not allocate codec.");
		
	packet = av_packet_alloc();
	if(!packet)
		throw std::runtime_error("Could not allocate packet.");
	
	ctx->width = res.get_width();
	ctx->height = res.get_height();
	ctx->bit_rate = 4000000;
	ctx->time_base = (AVRational){1, settings->get_fps()};
	//ctx->time_base = (AVRational){1, 1};
	ctx->framerate = (AVRational){settings->get_fps(), 1};
	ctx->framerate = (AVRational){1, 1};
	ctx->gop_size = 10; //?
	ctx->max_b_frames = 0; //?
	ctx->pix_fmt = AV_PIX_FMT_YUV420P; // ?
	
	/*
	if (codec->id == AV_CODEC_ID_H264)
		av_opt_set(ctx->priv_data, "preset", "slow", 0);
	*/
	int ret = avcodec_open2(ctx, codec, NULL);
	if(ret < 0)
	{
		throw std::runtime_error(std::to_string(ret));
	}
	
	f = fopen(filename.c_str(), "wb");
	if(!f)
		throw std::runtime_error("Error opening file " + filename.string());
	
	frame = av_frame_alloc();
	if(!frame)
		throw std::runtime_error("Could not allocate frame.");
	
	frame->format = ctx->pix_fmt;
	frame->width = ctx->width;
	frame->height = ctx->height;
	ret = av_frame_get_buffer(frame, 32);
	if(ret < 0)
		throw std::runtime_error("Could not get frambe buffer.");
}

PictureDumper::~PictureDumper()
{
	fclose(f);
	av_frame_free(&frame);
	av_packet_free(&packet);
	avcodec_free_context(&ctx);
}

void PictureDumper::addFrame(webcam::image * ptr)
{
	int ret = av_frame_make_writable(frame);
	if(ret < 0)
		throw std::runtime_error("av_make_frame_writable() failed");
	
	int width =	ctx->width;
	int height = ctx->height;
	unsigned char tmp1 = 0, tmp2 = 0;
	for(int y = 0; y < height; y++)
	{
		const unsigned char * data = ptr->get_data() + y*width * 2;
		for(int x = 0; x < width; x++)
		{			
			frame->data[0][y*frame->linesize[0] + x] = *(data + x*2);
		}
	}
	for(int y = 0; y < height/2; y++)
	{
		const unsigned char * data = ptr->get_data() + y*width*4;
		for(int x = 0; x < width/2; x++)
		{
			frame->data[1][y*frame->linesize[1] + x] = *(data + x*4+1);
			frame->data[2][y*frame->linesize[2] + x] = *(data + x*4+3);
		}
	}
	
	ret = avcodec_send_frame(ctx, frame);
	if(ret < 0)
		throw std::runtime_error("Error sending frame.");
		
	while(ret >= 0)
	{
		ret = avcodec_receive_packet(ctx, packet);
		if(ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			return;
		
		fwrite(packet->data, 1, packet->size, f);
		av_packet_unref(packet);
	}
}
