# v4l-surv

This program is intended to surveil an area using a webcam. It starts recording
when luminance changes sufficiently and keeps on recording while movement is
detected. It stops recording when no movement is detected for 3 seconds.


## Compile
Needs rojarand/libwebcam from GitHub, libpng-dev and libavcodec-dev to compile.

```
mkdir build; cd build
cmake ..
make
```

## Start
```
./v4l-test mpeg4
```

You may replace "mpeg4" with any ffmpeg-codec which supports the pixel-format
"yuv420p".
