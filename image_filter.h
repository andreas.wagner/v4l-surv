#ifndef _image_filter_h
#define _image_filter_h

#include <filesystem>
#include <libwebcam/webcam.h>
#include <png.h>

class ImageFilter
{
	public:
	ImageFilter(std::filesystem::path blind_map, webcam::video_settings * settings);
	~ImageFilter();
	
	bool process(webcam::image * ptr);
	
	private:
	unsigned char * mask;
	unsigned char * old_img;
	png_uint_32 width, height;
	int treshold;

};

#endif
