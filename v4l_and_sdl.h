#ifndef _v4l_and_sdl_h_
#define _v4l_and_sdl_h_

#include <string>
#include <filesystem>
#include <memory>

#include <libwebcam/webcam.h>


class VideoInputDevice
{
	public:
	VideoInputDevice();
	~VideoInputDevice();
	
	webcam::image * getImage();
	webcam::video_settings * getSettings();
	
	private:
	std::shared_ptr<webcam::device> device;
	webcam::video_settings video_settings;
};

#endif
