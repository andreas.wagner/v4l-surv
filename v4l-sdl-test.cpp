#include "v4l_and_sdl.h"
#include "picture-dumper.h"
#include "image_filter.h"

#include <signal.h>

#include <filesystem>
#include <sstream>
#include <memory>
#include <iostream>

volatile bool go_on = true;

void sig_int(int nr)
{
	nr;
	go_on = false;
}

int main(int argc, char *argv[])
{
	if(argc < 2)
		return -1;
	signal(SIGINT, &sig_int);

	std::stringstream filename;
	std::chrono::time_point start = std::chrono::system_clock::now();
	filename << std::chrono::system_clock::to_time_t(start) << ".avi";
	
	VideoInputDevice in;
	ImageFilter image_filter("test.png", in.getSettings());	
	std::shared_ptr<PictureDumper> pd = std::make_shared<PictureDumper>(filename.str(), argv[1], in.getSettings());
	
	auto last_iter = std::chrono::system_clock::now();
	bool recording = true;
	
	while(go_on)
	{
		webcam::image * ptr = in.getImage(); // allocates memory
		if(image_filter.process(ptr))
		{
			last_iter = std::chrono::system_clock::now();
		}
		
		if((std::chrono::system_clock::now() - last_iter) < std::chrono::seconds(3))
		{
			if(recording == false)
			{
				std::stringstream filename1;
				start = std::chrono::system_clock::now();
				filename1 << std::chrono::system_clock::to_time_t(start) << ".avi";
				std::cout << filename1.str() << std::endl;
	
				pd = std::make_shared<PictureDumper>(filename1.str(), argv[1], in.getSettings());
			}
			pd->addFrame(ptr);
			recording = true;
		}
		else
		{
			recording = false;
		}
		
		delete ptr;
	}	
}
